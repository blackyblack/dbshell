package dbshell;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import nrs.NxtException.NxtApiException;

import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.opencsv.CSVReader;

import blackyblack.Application;

public class DbApi
{
  public static String host;
  public static int port;
  
  static {
    host = Application.defaultNrsHost;
    if(Application.getBooleanProperty("blackyblack.isTestnet"))
    {      
      port = Application.defaultTestnetNrsPort;
    }
    else
    {
      port = Application.defaultNrsPort;
    }
  }
  
  public static String shell = "http://" + host + ":" + port + "/dbshell";
  
  public static class DbToResult
  {
    public String field;
    public int index;
    
    public DbToResult()
    {
      field = null;
      index = 0;
    }
  }
  
  public static String execute(String statement) throws NxtApiException
  {
    String content = "";
    List<BasicNameValuePair> fields = new ArrayList<BasicNameValuePair>();
    fields.add(new BasicNameValuePair("line", statement));
    
    CloseableHttpResponse response = null;
    try
    {
      try 
      {
        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(fields, "UTF-8");
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost http = new HttpPost(shell);
        http.setHeader("Origin", host);
        http.setEntity(entity);
        response = httpclient.execute(http);
        HttpEntity result = response.getEntity();
        content = EntityUtils.toString(result);
      }
      finally
      {
        if(response != null)
          response.close();
      }
    }
    catch (Exception e)
    {
      throw new NxtApiException(e.getMessage());
    }
    
    return content;
  }
  
  public static List<String[]> select(String statement) throws NxtApiException
  {
    List<String[]> result = new ArrayList<String[]>();
    String content = execute(statement);
    CSVReader reader = new CSVReader(new StringReader(content), '|');
    String [] nextLine;
    try
    {
      try
      {
        //skip 2 lines
        nextLine = reader.readNext();
        if(nextLine == null) return result;
        nextLine = reader.readNext();
        if(nextLine == null) return result;
        //read header
        nextLine = reader.readNext();
        if(nextLine == null) return result;
        
        while (true)
        {
          nextLine = reader.readNext();
          if(nextLine == null) break;
          if(nextLine.length < 1) continue;
          if(nextLine[0].equals("(data is partially truncated)")) continue;
          
          String[] clearData = new String[nextLine.length];
          for(int i = 0; i < nextLine.length; i++)
          {
            clearData[i] = nextLine[i].trim();
          }
          
          result.add(clearData);
        }
        
        if(result.size() > 0)
            result.remove(result.size() - 1);
      }
      finally
      {
        if(reader != null)
          reader.close();
      }
    }
    catch (IOException e)
    {
      throw new NxtApiException(e.getMessage());
    }
    
    return result;
  }
  
  public static List<String[]> select(String statement, String[] fields) throws NxtApiException
  {
    List<String[]> result = new ArrayList<String[]>();
    String content = execute(statement);
    CSVReader reader = new CSVReader(new StringReader(content), '|');
    String [] nextLine;
    DbToResult[] mapper = new DbToResult[100];
    try
    {
      try
      {
        //skip 2 lines
        nextLine = reader.readNext();
        if(nextLine == null) return result;
        nextLine = reader.readNext();
        if(nextLine == null) return result;
        //read header
        nextLine = reader.readNext();
        if(nextLine == null) return result;
        
        for(int i = 0; i < nextLine.length; i++)
        {
          for(int j = 0; j < fields.length; j++)
          {
            String dbField = nextLine[i];
            String field = fields[j];
            
            if(dbField == null) continue;
            if(field == null) continue;
            
            String a = dbField.toLowerCase().trim();
            String b = field.toLowerCase().trim();
            
            if(!a.equals(b)) continue;

            DbToResult map = new DbToResult();
            map.field = a;
            map.index = j;
            mapper[i] = map;
            break;
          }
        }
        
        while (true)
        {
          nextLine = reader.readNext();
          if(nextLine == null) break;
          if(nextLine.length < 1) continue;
          if(nextLine[0].equals("(data is partially truncated)")) continue;
          
          String[] clearData = new String[fields.length];
          for(int i = 0; i < nextLine.length; i++)
          {
            DbToResult map = mapper[i];
            if(map == null) continue;
            if(map.field == null) continue;
            
            clearData[map.index] = nextLine[i].toLowerCase().trim();
          }
          
          result.add(clearData);
        }
        
        if(result.size() > 0)
            result.remove(result.size() - 1);
      }
      finally
      {
        if(reader != null)
          reader.close();
      }
    }
    catch (IOException e)
    {
      throw new NxtApiException(e.getMessage());
    }
    
    return result;
  }
}
