package dbshell;

import java.util.ArrayList;
import java.util.List;

import nrs.NxtException.NxtApiException;
import nrs.Transaction;
import nrs.util.Convert;

/*
 * To have string representation of long variables use Convert.toUnsignedLong
 * To have proper long representation from string use Convert.parseUnsignedLong
 * */
public class TransactionTable
{
  public static List<Transaction> select(String statement) throws NxtApiException
  {
    //fix column order and count
    String selectPart = "SELECT * FROM TRANSACTION";
    String query = selectPart;
    if(statement != null && statement.length() > 0)
      query = selectPart + " " + statement;
    String[] fields = {"DB_ID", "ID", "RECIPIENT_ID", "AMOUNT", "HEIGHT", "BLOCK_ID", "TIMESTAMP", "TYPE", "SUBTYPE",
        "SENDER_ID", "BLOCK_TIMESTAMP", "REFERENCED_TRANSACTION_FULL_HASH", "VERSION", "HAS_MESSAGE", "HAS_ENCRYPTED_MESSAGE",
        "EC_BLOCK_HEIGHT", "EC_BLOCK_ID", "HAS_ENCRYPTTOSELF_MESSAGE", "TRANSACTION_INDEX", "PHASED",
        "HAS_PRUNABLE_MESSAGE", "HAS_PRUNABLE_ENCRYPTED_MESSAGE"};
    
    List<Transaction> result = new ArrayList<Transaction>();
      
    List<String[]> lines = DbApi.select(query, fields);
    for(String[] line : lines)
    {
      if(line == null) continue;
      if(line.length == 0) continue;
      if(line.length != fields.length) continue;
      
      try
      {
        Transaction item = new Transaction();
        item.deadline = 0;
        item.senderPublicKey = null;
        item.amountNQT = Long.parseLong(line[3]);
        item.feeNQT = 0L;
        item.type = Byte.parseByte(line[7]);
        item.subtype = Byte.parseByte(line[8]);
        item.version = Byte.parseByte(line[12]);
        item.timestamp = Integer.parseInt(line[6]);
        item.recipientId = Long.parseLong(line[2]);
        item.senderId = Long.parseLong(line[9]);
        item.referencedTransactionFullHash = line[11];
        item.signature = null;
        item.message = null;
        item.encryptedMessage = null;
        item.blockId = Long.parseLong(line[5]);
        item.height = Integer.parseInt(line[4]);
        item.id = Long.parseLong(line[1]);
        item.senderRS = Convert.rsAccount(item.senderId);
        item.blockTimestamp = Integer.parseInt(line[10]);
        item.fullHash = null;
        item.ecBlockHeight = Integer.parseInt(line[15]);
        item.ecBlockId = Long.parseLong(line[16]);
        item.appendages = null;
        item.appendagesSize = 0;
        result.add(item);
      }
      catch(Exception e)
      {
        //do nothing
      }
    }
    return result;
  }
}
