A tool for NRS dbshell accessing. Supports transaction table.
For manual dbshell access use http://localhost:7876/dbshell
Remote dbshell access requires admin password.

Libraries:

Opencsv 3.3: http://sourceforge.net/projects/opencsv/files/latest/download
JSON: http://json-simple.googlecode.com/files/json-simple-1.1.1.jar
Jetty
Apache HTTP components: https://hc.apache.org/downloads.cgi
Apache commons logging
Bouncycastle
Slf4j